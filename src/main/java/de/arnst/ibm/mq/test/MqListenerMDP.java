package de.arnst.ibm.mq.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class MqListenerMDP implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(MqListenerMDP.class);


    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                LOGGER.info(((TextMessage) message).getText());
            } catch (JMSException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            throw new IllegalArgumentException("Message must be of type TextMessage");
        }

    }
}