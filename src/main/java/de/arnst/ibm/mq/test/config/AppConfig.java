package de.arnst.ibm.mq.test.config;

import com.ibm.mq.jms.MQConnectionFactory;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import de.arnst.ibm.mq.test.MqListenerMDP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.SimpleMessageListenerContainer;

import javax.jms.JMSException;


@Configuration
public class AppConfig {

    @Autowired
    IbmMqConfig ibmMqConfig;

    @Bean
    public MQConnectionFactory createMQConnectionFactory() throws JMSException {
        MQConnectionFactory mqConnectionFactory;
        mqConnectionFactory = new MQQueueConnectionFactory();
        mqConnectionFactory.setHostName(ibmMqConfig.getHostName());
        mqConnectionFactory.setPort(ibmMqConfig.getPort());
        mqConnectionFactory.setQueueManager(ibmMqConfig.getQueueManager());
        mqConnectionFactory.setChannel(ibmMqConfig.getChannel());
        mqConnectionFactory.setTransportType(WMQConstants.WMQ_CM_CLIENT);
        mqConnectionFactory.setStringProperty(WMQConstants.USERID, ibmMqConfig.getUser());
        mqConnectionFactory.setStringProperty(WMQConstants.PASSWORD, ibmMqConfig.getPassword());
        return mqConnectionFactory;
    }

    @Bean(name = "jmsCachingContainerFactory")
    public CachingConnectionFactory createCachingConnectionFactory(final MQConnectionFactory mqConnectionFactory) {
        CachingConnectionFactory factory = new CachingConnectionFactory(mqConnectionFactory);
        factory.setSessionCacheSize(13);
        factory.setCacheConsumers(false);
        return factory;
    }

    @Bean(name = "jmsListenerContainerFactory")
    public JmsListenerContainerFactory<?> createJmsListenerContainerFactory(final CachingConnectionFactory mqCachingConnectionFactory) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(mqCachingConnectionFactory);
        factory.setReceiveTimeout(Long.valueOf(ibmMqConfig.getReceiveTimeout()));
        return factory;
    }

    @Autowired
    MqListenerMDP mqListenerMDP;


    @Bean(name = "jmsListenerContainerFactoryMDP")
    public SimpleMessageListenerContainer createJmsListenerContainerFactoryMDP(final CachingConnectionFactory mqCachingConnectionFactory) {
        SimpleMessageListenerContainer factory = new SimpleMessageListenerContainer();
        factory.setConnectionFactory(mqCachingConnectionFactory);
        factory.setMessageListener(mqListenerMDP);
        factory.setDestinationName("DEV.QUEUE.1");
        factory.setConcurrentConsumers(ibmMqConfig.getConcurrentConsumer());
        return factory;
    }

    @Bean
    public JmsTemplate jmsTemplate(final CachingConnectionFactory mqCachingConnectionFactory) {
        return new JmsTemplate(mqCachingConnectionFactory);
    }

}
