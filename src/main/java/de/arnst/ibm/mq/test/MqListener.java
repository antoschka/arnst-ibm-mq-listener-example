package de.arnst.ibm.mq.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Profile("activeDefaultListener")
public class MqListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(MqListener.class);

    // A queue from the default MQ Developer container config
    static final String qName = "DEV.QUEUE.2";
    int i = 0;

    @JmsListener(destination = qName, concurrency = "8-10")
    public void receiveMessage(String msg) {
        i = i + 1;
        LOGGER.info("Queueing Messages: " + i);
    }
}
